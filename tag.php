//1.standard tag

<?php
echo"<br>Default tag<br><br><br>" ;
?>

//2.pure php code
<?php

echo"<br>pure php tag<br><br>";

?>
3.//short open tag
<?php
echo "<br>short open tags<br><br>";
?>

//4.html script tags

<script language="php">
echo "<br><br>This is HTML script tags.<br><br>";
</script>

5.//asp style tags
<%
echo '<br><br>asp tags<br><br>';
%>

6.//valid code

<?php
echo '<br><br>This is a test string<br><br>';
?>
7.//php case sensivity
<?php
echo("We are learning PHP case sensitivity <br />");
ECHO("We are learning PHP case sensitivity <br />");
EcHo("We are learning PHP case sensitivity <br />");
?>




<?php
echo "<h2>PHP is Fun!</h2>";
echo "Hello world!<br>";
echo "I'm about to learn PHP!<br>";
echo "This ", "string ", "was ", "made ", "with multiple parameters.";
?>




<?php
$txt1 = "Learn PHP";
$txt2 = "W3Schools.com";
$x = 5;
$y = 4;

echo "<h2>$txt1</h2>";
echo "Study PHP at $txt2<br>";
echo $x + $y;
?>
